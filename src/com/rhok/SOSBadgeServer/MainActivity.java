package com.rhok.SOSBadgeServer;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

@SuppressLint("NewApi")
public class MainActivity extends Activity {
    private static String LOG_TAG = MainActivity.class.getName();

    private static final int UDP_SERVER_PORT = 2004;
    private static final int MAX_UDP_DATAGRAM_LEN = 1500;
    private TextView textMessage;

	private Button smsButton;
    private AlertNotifier notifier;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

        // Sms notification
        notifier = new AlertNotifier(this);
	}

    public void alarm(View view) {
        Log.i(LOG_TAG, "Creating ClientServerActivity");
        notifier.alarm();
    }

    /** Called when the user clicks the Go to Server button */
    public void goToServer(View view) {
        Intent myIntent = new Intent(this, ClientServerActivity.class);
        startActivity(myIntent);
    }

	public void addListenerOnButton() {
		smsButton = (Button) findViewById(R.id.button);

		smsButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		return true;
	}

	@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_settings:
                Intent i = new Intent(this, SettingsActivity.class);
                startActivity(i);
                break;
        }
        return true;
    }
}
