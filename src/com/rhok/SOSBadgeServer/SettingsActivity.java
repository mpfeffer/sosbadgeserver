package com.rhok.SOSBadgeServer;

import java.util.List;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceClickListener;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;

/**
 * A {@link PreferenceActivity} that presents a set of application settings. On
 * handset devices, settings are presented as a single list. On tablets,
 * settings are split by category, with category headers shown to the left of
 * the list of settings.
 * <p>
 * See <a href="http://developer.android.com/design/patterns/settings.html">
 * Android Design: Settings</a> for design guidelines and the <a
 * href="http://developer.android.com/guide/topics/ui/settings.html">Settings
 * API Guide</a> for more information on developing a Settings UI.
 */
public class SettingsActivity extends PreferenceActivity {
	protected static final String KEY_PREF_EMERGENCY_NUMBER = "emergency_contact";

	/** {@inheritDoc} */
	@Override
	public boolean onIsMultiPane() {
		return isXLargeTablet(this);
	}

	/**
	 * Helper method to determine if the device has an extra-large screen. For
	 * example, 10" tablets are extra-large.
	 */
	private static boolean isXLargeTablet(Context context) {
		return (context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_XLARGE;
	}

	/** {@inheritDoc} */
	@Override
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void onBuildHeaders(List<Header> target) {
		loadHeadersFromResource(R.xml.pref_headers, target);
	}

	protected boolean isValidFragment(String fragmentName) {
		if (GeneralPreferenceFragment.class.getName().equals(fragmentName))
			return true;
		return false;
	}

	/**
	 * This fragment shows general preferences only. It is used when the
	 * activity is showing a two-pane settings UI.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public static class GeneralPreferenceFragment extends PreferenceFragment {
		protected static final int PICK_CONTACT = 1;

		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			addPreferencesFromResource(R.xml.pref_general);

			// Get Custom contact Pref
			Preference customContact = (Preference) findPreference(KEY_PREF_EMERGENCY_NUMBER);
			customContact
					.setOnPreferenceClickListener(new OnPreferenceClickListener() {

						@Override
						public boolean onPreferenceClick(Preference preference) {
							Intent i = new Intent(Intent.ACTION_PICK,
									ContactsContract.Contacts.CONTENT_URI);
							startActivityForResult(i, PICK_CONTACT);
							return true;
						}
					});
		}

		@Override
		public void onActivityResult(int requestCode, int resultCode,
				Intent data) {
			if (requestCode == PICK_CONTACT) {
				if (data != null) {
					Uri result = data.getData();
					String id = result.getLastPathSegment();

					Cursor cursor = getActivity().getContentResolver().query(
							Phone.CONTENT_URI, null, Phone.CONTACT_ID + " = ?",
							new String[] { id }, null);

					int phoneIdx = cursor.getColumnIndex(Phone.NUMBER);
					if (cursor.moveToFirst()) {
						String phone = cursor.getString(phoneIdx);

						Preference preference = findPreference(KEY_PREF_EMERGENCY_NUMBER);
						preference.getEditor()
								.putString(preference.getKey(), phone).commit();
						preference.setSummary(phone);

					}
				}
			}
		}
	}

}
