package com.rhok.SOSBadgeServer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.telephony.SmsManager;

/**
 * Created by schmidtni on 07.12.13.
 */
public class AlertNotifier {

    private final Context ctx;

    AlertNotifier(Context ctx) {
        this.ctx = ctx;
    }

    public void alarm() {
        SharedPreferences sharedPref = PreferenceManager
                .getDefaultSharedPreferences(ctx);
        String emergencyNumber = sharedPref.getString(
                SettingsActivity.KEY_PREF_EMERGENCY_NUMBER, "");

        if (!"".equals(emergencyNumber)) {
            SmsManager sms = SmsManager.getDefault();
            sms.sendTextMessage(emergencyNumber, null,
                    "SMS! I urgently need help!", null, null);
        } else {
            Dialog dialog = new AlertDialog.Builder(ctx)
                    .setMessage(R.string.phone_missing).create();
            dialog.show();
        }
    }
}
