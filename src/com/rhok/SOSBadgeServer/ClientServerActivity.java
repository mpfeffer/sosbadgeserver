package com.rhok.SOSBadgeServer;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class ClientServerActivity extends Activity implements OnClickListener {
    private static String LOG_TAG = ClientServerActivity.class.getName();

    public static final String SERVERIP = "192.168.43.1"; // ‘Within’ the emulator!
    public static final int SERVERPORT = 3769;
    public static final int CLIENTPORT = 3769;
    public TextView text;
    public EditText input;
    public Button btn;
    public Handler Handler;
    private boolean start = false;
    private AlertNotifier notifier;
    private Thread server = null;

    /** Called when the activity is first created. */

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        Log.i(LOG_TAG, "Creating ClientServerActivity");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server);

        // Sms notification
        notifier = new AlertNotifier(this);

        // Other stuff
        text =(TextView)findViewById(R.id.textView);
        input=(EditText)findViewById(R.id.editText);
        btn = (Button)findViewById(R.id.button);
        btn.setOnClickListener(this);

        if (server == null) {
            server = new Thread(new Server());
        }
        if (! server.isAlive()) {
            server.start();
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) { }
        }

        Handler = new Handler() {
            @Override public void handleMessage(Message msg) {
                String text = (String)msg.obj;
                ClientServerActivity.this.text.append(text);
            }
        };
    }

    @Override
    public void onClick(View v) {
        new Thread(new Client()).start();
        start = true;
    }

    public class Client implements Runnable {
        @Override
        public void run() {
            while (!start) {}
            try {
                Thread.sleep(500);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
            try {
                InetAddress serverAddr = InetAddress.getByName(SERVERIP);
                updatetrack("Client: Start connecting\n");
                DatagramSocket socket = new DatagramSocket();
                byte[] buf;
                if(!input.getText().toString().isEmpty())
                {
                    buf=input.getText().toString().getBytes();
                }
                else
                {
                    buf = ("Default message").getBytes();
                }
                DatagramPacket packet = new DatagramPacket(buf,


                        buf.length, serverAddr, CLIENTPORT);
                updatetrack("Client: Sending ‘" + new String(buf) + "\n");
                socket.send(packet);
                updatetrack("Client: Message sent\n");
                updatetrack("Client: Succeed!\n");
            } catch (Exception e) {
                updatetrack("Client: Error!\n");
            }
        }
    }
    public class Server implements Runnable {

        @Override
        public void run() {
            while (!start) {}

            DatagramSocket socket = null;
            try {
                updatetrack("nServer: Start connecting\n");
                socket = new DatagramSocket(SERVERPORT);
                byte[] buf = new byte[255];
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                while (true) {
                    updatetrack("Server: Waiting for messages.\n");
                    socket.receive(packet);
                    updatetrack("Server: Message received: '" + packet.getData()[0] + packet.getData()[1] + packet.getData()[2] + "'\n");
                    parsePacket(packet.getData());
                }
            } catch (Exception e) {
                updatetrack("Server: Error!\n");
            } finally {
                if (socket != null) {
                    socket.close();
                }
            }
        }
    }

    public void parsePacket(byte[] packet) {
        if (packet[0] == -83 && packet[2] == 1) {
            notifier.alarm();
        }
    }

    public void updatetrack(String s){
        Message msg = new Message();
        String textTochange = s;
        msg.obj = textTochange;
        Handler.sendMessage(msg);
    }

}